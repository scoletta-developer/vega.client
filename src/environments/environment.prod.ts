export const environment = {
  production: true,
  VERSION: require('../../package.json').version,
  GA_TRACKING_CODE: 'UA-134452270-1'
};
