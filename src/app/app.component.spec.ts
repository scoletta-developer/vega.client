import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { SwUpdate, ServiceWorkerModule, UpdateAvailableEvent } from '@angular/service-worker';
import { of } from 'rxjs';
import {RouterTestingModule} from '@angular/router/testing';
import { Component } from '@angular/core';

@Component({selector: 'vega-loader', template: ''})
class LoaderComponent { }

@Component({selector: 'vega-header', template: ''})
class HeaderComponent { }

@Component({selector: 'vega-footer', template: ''})
class FooterComponent { }

describe('AppComponent', () => {

  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;
  const swUpdateMock = jasmine.createSpyObj('SwUpdate', [
    'checkForUpdate',
    'isEnabled',
    'available'
  ]);
  let windowSpy: jasmine.Spy;

  beforeEach(async(() => {

    swUpdateMock.available = of<UpdateAvailableEvent>(null);
    swUpdateMock.isEnabled = true;
    swUpdateMock.checkForUpdate.and.returnValue(Promise.resolve());

    windowSpy = spyOn(window, 'confirm');

    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        LoaderComponent,
        HeaderComponent,
        FooterComponent
      ],
      imports: [
        RouterTestingModule,
        ServiceWorkerModule.register('', {enabled: false})
      ],
      providers: [
        { provide: SwUpdate, useValue: swUpdateMock }
      ]
    });

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  }));

  it('should create the app', () => {
    expect(component).toBeTruthy();
    expect(component).toBeDefined();
  });

  it('should log an error if check of update fails', async () => {

    windowSpy.and.returnValue(false);
    spyOn(console, 'error');
    swUpdateMock.checkForUpdate.and.returnValue(Promise.reject());

    component.ngOnInit();

    expect(await console.error).toHaveBeenCalled();
  });
});
