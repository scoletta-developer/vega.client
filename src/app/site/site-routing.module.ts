import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent } from './pages/home/home.component';
import { AboutComponent } from './pages/about/about.component';
import { ContactsComponent } from './pages/contacts/contacts.component';
import { ExperiencesComponent } from './pages/experiences/experiences.component';

const siteRoutes: Routes = [
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'experiences',
    component: ExperiencesComponent
  },
  {
    path: 'contacts',
    component: ContactsComponent
  },
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(siteRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class SiteRoutingModule { }
