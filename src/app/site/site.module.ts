import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SiteRoutingModule } from './site-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { SharedModule } from '../shared/shared.module';
import { AboutComponent } from './pages/about/about.component';
import { ExperiencesComponent } from './pages/experiences/experiences.component';
import { ContactsComponent } from './pages/contacts/contacts.component';

@NgModule({
  declarations: [HomeComponent, AboutComponent, ExperiencesComponent, ContactsComponent],
  imports: [
    CommonModule,
    SiteRoutingModule,
    SharedModule
  ]
})
export class SiteModule { }
