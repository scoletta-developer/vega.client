import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'vega-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  trip: string;

  constructor() { }

  ngOnInit() {
    this.trip = 'https://en.wikipedia.org/wiki/Japan';
  }

}
