import { Component, OnInit } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { Router, NavigationEnd, RouterEvent } from '@angular/router';

@Component({
  selector: 'vega-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  route: string;
  isPageNotFound: boolean;

  public ngOnInit(): void {
    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(() => {
        if (confirm('New version available. Load New Version?'))
          location.reload();
      });
      this.swUpdate.checkForUpdate().catch((err) => {
        console.error('error when checking for update', err);
      });
    }
  }

  constructor(private swUpdate: SwUpdate, private _router: Router) {

    this.route = this._router.url;

    this._router.events.subscribe((event: RouterEvent) => {

      if (event instanceof NavigationEnd ) {
        this.route = event.url;
        this.isPageNotFound = event.urlAfterRedirects === '/404';
      }
    });
  }
}
