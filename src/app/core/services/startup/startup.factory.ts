import { StartupService } from './startup.service';

export function startUpApp(startupService: StartupService) {
    return () => startupService.startup();
}
