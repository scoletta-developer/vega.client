import { TestBed } from '@angular/core/testing';
import { StartupService } from './startup.service';
import { startUpApp } from './startup.factory';

describe('Startup Factory', () => {

  let startupServiceSpy: jasmine.SpyObj<StartupService>;

  beforeEach(() => {

    const spy = jasmine.createSpyObj('StartupService', ['setVersion']);

    TestBed.configureTestingModule({
      providers: [
        { provide: StartupService, useValue: spy }
      ]
    });

    startupServiceSpy = TestBed.get(StartupService);
  });

  it('should expose a function to get the version', () => {

    expect(startUpApp(startupServiceSpy)).toEqual(jasmine.any(Function));
  });
});
