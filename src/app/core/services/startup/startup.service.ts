import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { SwPush } from '@angular/service-worker';
import { GoogleAnalitycsServiceService } from '../google-analitycs-service/google-analitycs-service.service';

@Injectable({
  providedIn: 'root'
})
export class StartupService {

  constructor(public swPush: SwPush, private _googleAnalitycs: GoogleAnalitycsServiceService) { }

  startup(): Promise<any> {
    return Promise.all(
      [
        this.setVersion(),
        this._googleAnalitycs.addScripts()
      ]
    );
  }

  setVersion(): Promise<any> {
    return new Promise((resolve, reject) => {

      try {

        const g = 'color:#0e4c67;font-weight:bold;';

        console.log(`
%cSimone Coletta Website | Frontend Developer
VERSION ${environment.VERSION}

For bug report please write to: scoletta.developer@gmail.com

Service worker ${this.swPush.isEnabled ? 'enabled' : 'disabled'}
        `, g);

        resolve(true);
      } catch (error) {

        reject();
      }
    });
  }
}
