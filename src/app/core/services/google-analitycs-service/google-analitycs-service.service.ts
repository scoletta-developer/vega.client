import { Injectable, Inject } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class GoogleAnalitycsServiceService {

  constructor(@Inject(DOCUMENT) private document: Document) { }

  addScripts() {
    return new Promise((resolve, reject) => {
      const head = this.document.getElementsByTagName('head')[0];

      const googleAnalyticsFirstScript = this.document.createElement('script');
      googleAnalyticsFirstScript.async = true;
      googleAnalyticsFirstScript.src = 'https://www.googletagmanager.com/gtag/js?id=' + environment.GA_TRACKING_CODE;

      const googleAnalyticsSecondScript = this.document.createElement('script');
      googleAnalyticsSecondScript.innerHTML = '    window.dataLayer = window.dataLayer || [];\n' +
        '    function gtag(){dataLayer.push(arguments);}\n' +
        '    gtag(\'js\', new Date());\n' +
        '\n' +
        '    gtag(\'config\', \'' + environment.GA_TRACKING_CODE + '\', { \'send_page_view\': false });';

      head.insertBefore(googleAnalyticsSecondScript, head.firstChild);
      head.insertBefore(googleAnalyticsFirstScript, head.firstChild);

      resolve(true);
    });
  }
}
