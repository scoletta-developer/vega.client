import { TestBed } from '@angular/core/testing';

import { GoogleAnalitycsServiceService } from './google-analitycs-service.service';

describe('GoogleAnalitycsServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GoogleAnalitycsServiceService = TestBed.get(GoogleAnalitycsServiceService);
    expect(service).toBeTruthy();
  });
});
