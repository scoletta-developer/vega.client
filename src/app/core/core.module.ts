import { NgModule, Optional, SkipSelf, ModuleWithProviders, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GtagModule, Gtag } from 'angular-gtag';
import { startUpApp } from './services/startup/startup.factory';
import { StartupService } from './services/startup/startup.service';
import { environment } from '../../environments/environment';
import { GoogleAnalitycsServiceService } from './services/google-analitycs-service/google-analitycs-service.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    GtagModule.forRoot({trackingId: environment.GA_TRACKING_CODE, trackPageviews: true })
  ]
})
export class CoreModule {

  constructor (@Optional() @SkipSelf() parentModule: CoreModule, gtag: Gtag) {
    if (parentModule)
      throw new Error('CoreModule is already loaded. Import it in the AppModule only');
  }

  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        { provide: APP_INITIALIZER, useFactory: startUpApp, deps: [StartupService], multi: true},
        GoogleAnalitycsServiceService
      ]
    };
  }
}
