import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { LoaderComponent } from './components/loader/loader.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { MenuComponent } from './components/menu/menu.component';
import { RouterModule } from '@angular/router';
import { ReposComponent } from './components/repos/repos.component';
import { ReferenceComponent } from './components/reference/reference.component';

@NgModule({
  declarations: [
    PageNotFoundComponent,
    LoaderComponent, FooterComponent, HeaderComponent, MenuComponent, ReposComponent, ReferenceComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    LoaderComponent,
    HeaderComponent,
    FooterComponent
  ]
})
export class SharedModule { }
