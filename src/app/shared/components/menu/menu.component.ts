import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MenuElement } from '../../models/menu-element';

@Component({
  selector: 'vega-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  @Input()
  menuVoices: Array<MenuElement>;

  @Input()
  mobileMenuOpen: boolean;

  @Output()
  toggledMenu = new EventEmitter<void>();

  constructor() { }

  ngOnInit() { }

  toggleMenu() {
    this.toggledMenu.emit();
  }
}
