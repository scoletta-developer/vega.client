import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MenuElement } from '../../models/menu-element';
import { RouterTestingModule } from '@angular/router/testing';
import { Router, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs';

@Component({selector: 'vega-menu', template: ''})
class MenuComponent {
  @Input() menuVoices: Array<MenuElement>;

  @Input() mobileMenuOpen: boolean;

  @Output() toggledMenu = new EventEmitter<void>();
}

@Component({selector: 'vega-repos', template: ''})
class ReposComponent { }

@Component({selector: 'vega-reference', template: ''})
class ReferenceComponent { }

class MockRouter {
  public ne = new NavigationEnd(0, '/', '/');
  public events = new Observable(observer => {
    observer.next(this.ne);
    observer.complete();
  });
  public url = '/test';
}

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HeaderComponent,
        MenuComponent,
        ReposComponent,
        ReferenceComponent
      ],
      providers: [
        { provide: Router, useClass: MockRouter }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {

    expect(component).toBeTruthy();
  });
});
