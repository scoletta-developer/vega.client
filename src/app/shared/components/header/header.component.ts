import { Component, OnInit } from '@angular/core';
import { MenuElement } from '../../models/menu-element';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';

@Component({
  selector: 'vega-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  enableReference: boolean;

  mobileMenuOpen: boolean;

  menuVoices: Array<MenuElement> = [
    {
      target: '/',
      text: 'home'
    },
    {
      target: '/about',
      text: 'about'
    },
    {
      target: '/experiences',
      text: 'experiences'
    }
  ];

  constructor(private _router: Router) {
  }

  ngOnInit() {

    this.enableReference = this._router.url !== '/';

    this._router.events.subscribe((event: RouterEvent) => {

      if (event instanceof NavigationEnd )
        this.enableReference = event.url !== '/';
    });

    this.mobileMenuOpen = false;
  }

  onToggleMenu() {
    this.mobileMenuOpen = !this.mobileMenuOpen;
  }
}
